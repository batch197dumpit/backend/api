const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "Full Name is required"]
	},
	verified: {
		type: Boolean,
		default: false
	},
	email: {
		type: String,
		required: [true, 'Email address is required!']
	},
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Ordered"
			}
		}
	]
});

module.exports = mongoose.model('User', userSchema) || mongoose.models.User;