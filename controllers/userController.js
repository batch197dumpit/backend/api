const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const userOtpVerification = require('../models/userOtpVerification')

// env variables
require("dotenv").config();

const nodemailer = require("nodemailer")
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth:{
    user: process.env.AUTH_EMAIL,
    pass: process.env.AUTH_PASS
  }
})

// testing nodemailer success
transporter.verify((error, success) => {
  if(error){
    console.log(error);
  }else{
    console.log("ready for messages");
    console.log(success)
  }
})


//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {	
		if (result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// User registration
module.exports.registerUser = (reqBody) => {	
	
	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10) 
	})	

	
	return newUser.save().then((user, error) => {
				
		if(error) {
			return false
		} else {

			const otp = `${Math.floor(1000+Math.random()*9000)}`;
			const mailOptions = {
				from: process.env.AUTH_EMAIL,
				to: reqBody.email,
				subject: "Verify your email",
				html: `<p>Enter <b>${otp}</b>  in the app to verify your email address.This code <b>expires in 1 hour</b></p>`
	
			};
			// const saltRounds = 10;
			// const hashedOTP = bcrypt.hashSync(otp,saltRounds);
			
			let newOTPVerification =  new userOtpVerification ({
				_id:newUser.id, 
				email: reqBody.email,
				otp: otp,
				createdAt: Date.now(),
				expiresAt: Date.now() + 3600000
			});

			newOTPVerification.save();
			transporter.sendMail(mailOptions);

			// return "Hello "+ reqBody.name +", you have successfully registered please verify your email before logging in"// true
			return true


		}
	})

};

module.exports.verifyUser = (reqBody) => {
	
	return userOtpVerification.find({email: reqBody.email}).then(result => {	
		if (reqBody.email == result.email && reqBody.otp == result.otp) {
				result.verified = true;
				result.save();
			return true
		} else {
			return false
		}
	})
};

// // verify User
// module.exports.verifyUser = (userId,reqBody) => {

	
// 	return userOtpVerification.findById(userId).then((result,error) => {
		
// 		// const compareResult = bcrypt.compare(reqBody.otp,result.otp);

// 		if(reqBody.email == result.email && reqBody.otp == result.otp){
			
// 			 User.findOne({email: result.email}).then((result,error) => {
				

// 				result.verified = true;
// 				result.save();
				
// 			 });
// 			//  return "Your account has been verified, you can now login your account "
// 			 return true
// 		}else{
// 			// return "Please check your email and one time password"
// 			return false
// 		}
// 	})
// }


// User login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// All Users details
module.exports.getProfile = (userData) => {
	return User.findById(userData.userId).then(result => {
		console.log(userData)
		if (result == null) {
			return false
		} else {
		console.log(result)
		result.password = "*****";
		return result;
		}
	});
};

module.exports.order = async (data) => {
	if (data.isAdmin == true) {
		return false
	} else {
		let isUserUpdated = await User.findById(data.userId).then(user => {
			user.orders.push({productId: data.productId});
			return user.save().then((user, error) => {
				if(error) {
					return false
				} else {
					return true
				}
			})
		})

		let isProductUpdated = await Product.findById(data.productId).then(product => {
			product.orderedProduct.push({userId: data.userId});
			return product.save().then((product, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})
		})

		if (isUserUpdated && isProductUpdated) {
			return true
		} else {
			return false
		}
	}
};

// Set user to admin
// async function setUserAdmin (req, res, next) {
// 	const user = auth.decode(req.headers.authorization);
// 	if (user.isAdmin) {
// 		try {
// 			result = await User.findByIdAndUpdate(req.body.userId, {isAdmin: req.body.isAdmin});
// 			return res.send('Change status priviledge successful.');
// 		} catch (error) {
// 			next (error);
// 		};
// 	} else {
// 		return res.send('Not authorized to change status.')
// 	};
// };
	
// module.exports = {
// 	setUserAdmin: setUserAdmin
// };