const Product = require('../models/Product');
const User = require('../models/User');
const auth = require('../auth');

// create product (admin only)
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newProduct.save().then((product, error) => {		
		if(error) {
			return false
		} else {
			return true
		}
	});
};

// Retrieve all products
module.exports.getAllProducts = (userData) => {
	console.log(userData)
	if (userData.isAdmin) {
			return Product.find({}).then((result, error) => {
				return result;
				// return error ? false : "Success";
		})
	} else {
		// return false
		return Promise.resolve(false)
	}
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})

};

//Retrieve All Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
};

// Update product information (admin only)
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) =>{
		if(error) {
			return false
		} else {
			return true		
		}
	})
};

// Archive product (admin only)
module.exports.archiveProduct = (data) => {
	return Product.findById(data.productId).then((result, error) => {
		if(data.isAdmin === true) {
			result.isActive = false;
			return result.save().then((archivedProduct, error) => {
				if(error) {
					return false;
				} else {
					return true;
				}
			})
		} else {
			return false
		}
	})
}

module.exports.activeProduct = (data) => {
	return Product.findById(data.productId).then((result, err) => {
		if(data.isAdmin === true) {
			result.isActive = true;
			return result.save().then((activeProduct, err) => {
				if(err) {
					return false;
				} else {
					return true;
				}
			})
		} else {
			return false
		}
	})
}


