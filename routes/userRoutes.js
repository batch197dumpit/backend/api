const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for register
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for login authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user verification
router.put("/verify", (req, res) => {
    userController.verifyUser(req.params.id,req.body).then(resultFromController => res.send(resultFromController))
  });

// Route for all users details
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});

// route to add to cart
router.post("/order", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.body.productId
    }

    userController.order(data).then(resultFromController =>res.send(resultFromController))
});


module.exports = router;