// setup node js (express, mongoose, cors)
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// routes requirement here
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
// const orderRoutes = require('./routes/orderRoutes');

// end route requirement

const app = express();

const port = process.env.PORT || 5000;

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI here:
app.use('/users', userRoutes);
app.use('/products', productRoutes);
// app.use('/orders', orderRoutes);

// end Main URI

// mongoose connection
mongoose.connect(`mongodb+srv://elaydumpit:admin123@zuitt-batch197.pa6lt4y.mongodb.net/capstone?retryWrites=true&w=majority`,{

  useNewUrlParser: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', () => {
	console.log('Connection Error');
});
db.once('open', () => {
	console.log('Connected to MongoDB!');
});

app.listen(port, () => {
	console.log(`API is now online at port ${port}.`);
});